//Code Js pour le tp
$(document).ready(function()
{
    var price=0;
    
    $('.pizza-type label').hover(

        function () {
            $(this).find(".description").show();
        },
        function () {
            $(this).find(".description").hide();
        }
    )

    
    $('.nb-parts input').on('keyup', function () {

        const pizza = $('<span class="pizza-pict"></span>');
        nbParts = +$(this).val();

        $(".pizza-pict").remove();

        for(var i = 0; i<nbParts/6 ; i++){

            $(this).after(pizza.clone().addClass("pizza-6"));
        }

        if(nbParts%6 !== 0) $('.pizza-pict').last().removeClass('pizza-6').addClass('pizza-' + nbParts%6);
        price();
    })

    
    
    $('.next-step').click(function () {
        $('.infos-client').slideDown();
        $(this).hide();
    })

    
    
    const input = $('<br><input type="text"/>');

    $('.add').click(function () {
        $(this).before(input.clone());
    })

    $('.done').click(function () {

        const name = $('#name').val();
        const thanks = $('<span>Merci ' + name + ' ! Votre commande sera livrée dans 15 minutes</span>');
        $('.main').slideUp();
        $('.headline').append(thanks);
    })
    
	$('input').change(function() {
        charPrice='0';
        intValue=0;
        charPrice=$('input[name=type]:checked').attr('data-price');
        price=parseInt(charPrice);
        if($('input[name=pate]:checked')&&($('input[name=pate]:checked').attr('data-price')!=undefined)){
            charPrice=$('input[name=pate]:checked').attr('data-price');
            intValue=parseInt(charPrice);
            price=intValue+price;
        }
        if($('input[name=extra]:checked')&&($('input[name=extra]:checked').attr('data-price')!=undefined)){
            charPrice=$('input[name=extra]:checked').attr('data-price');
            intValue=parseInt(charPrice);
            price=intValue+price;
        }

        $('.tile p').replaceWith("<p> "+ price + " € </p>");
	});
    
    
});